package com.anadollu.mailo.util;

import org.subethamail.smtp.auth.LoginFailedException;
import org.subethamail.smtp.auth.UsernamePasswordValidator;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class SMTPAuthentication implements UsernamePasswordValidator {
    private String username;
    private String password;

    public SMTPAuthentication(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void login(String username, String password) throws LoginFailedException {
        if (this.username.equals(username) && this.password.equals(password)) {
            return;
        } else {
            throw new LoginFailedException();
        }
    }

}
