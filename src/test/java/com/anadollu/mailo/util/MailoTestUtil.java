package com.anadollu.mailo.util;

import java.io.IOException;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class MailoTestUtil {
    public static String getAddressHeader(String address) {
        return address;
    }

    public static String getAddressHeader(String name, String address) {
        return name + " <" + address + ">";
    }

    public static String getStringContent(MimeMultipart mmp, int index) throws IOException, MessagingException {
        return getStringContent(mmp.getBodyPart(index));
    }

    public static String getStringContent(BodyPart bodyPart) throws IOException, MessagingException {
        return (String) bodyPart.getContent();
    }
}
