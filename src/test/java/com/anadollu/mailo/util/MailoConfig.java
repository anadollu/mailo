package com.anadollu.mailo.util;

import com.anadollu.mailo.api.configuration.SessionConfig;
import com.anadollu.mailo.api.configuration.SessionConfigBuilder;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class MailoConfig {

    public static final SessionConfig STANDART_CONFIG;
    public static final SessionConfig GMAIL_CONFIG;

    static {
        STANDART_CONFIG = SessionConfigBuilder
                .newInstance()
                .serverHost("localhost")
                .serverPort(25252)
                // .from("no-reply@iq4j.com")
                // .fromName("Mailo Test")
                // .replyTo("no-reply@iq4j.com")
                // .replyToName("Do not Reply!!")
                .build();

        GMAIL_CONFIG = SessionConfigBuilder
                .newInstance()
                .serverHost("smtp.gmail.com")
                .serverPort(587)
                .username("no-reply@iq4j.com")
                .password("**********")
                .enableTls(true)
                .auth(true)
                .requireTls(true)
                .from("no-reply@iq4j.com")
                .fromName("Mailo Test")
                .replyTo("no-reply@iq4j.com")
                .replyToName("Do not Reply!!")
                .build();

    }

}
