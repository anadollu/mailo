package com.anadollu.mailo;

import com.anadollu.mailo.api.EmailContact;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class EmailContactTestImpl implements EmailContact {
    private String name;
    private String email;

    public EmailContactTestImpl() {

    }

    public EmailContactTestImpl(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return getEmail();
    }
}
