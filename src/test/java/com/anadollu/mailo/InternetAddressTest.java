package com.anadollu.mailo;

import com.anadollu.mailo.api.EmailContact;
import com.anadollu.mailo.api.InvalidAddressException;
import com.anadollu.mailo.api.MailMessage;
import com.anadollu.mailo.impl.BasicEmailContact;
import com.anadollu.mailo.impl.MailMessageImpl;
import com.anadollu.mailo.util.MailoConfig;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class InternetAddressTest {

    @Test
    public void validAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        BasicEmailContact mailo = new BasicEmailContact("mailo1@anadollu.com");
        BasicEmailContact mailor = new BasicEmailContact("mailor@domain.test");

        Collection<EmailContact> addresses = new ArrayList<EmailContact>();
        addresses.add(mailor);

        m.from("mailo1@anadollu.com", "Mail Mailo<mailo1@anadollu.com>");
        m.from("Mail Mailo<mailo1@anadollu.com>");
        m.from("mailo1@anadollu.com");
        m.from(mailo);
        m.from(addresses);

        m.to("mailo1@anadollu.com", "Mail Mailo<mailo1@anadollu.com>");
        m.to("Mail Mailo<mailo1@anadollu.com>");
        m.to("mailo1@anadollu.com");
        m.to(mailo);
        m.to(addresses);

        m.cc("mailo1@anadollu.com", "Mail Mailo<mailo1@anadollu.com>");
        m.cc("Mail Mailo<mailo1@anadollu.com>");
        m.cc("mailo1@anadollu.com");
        m.cc(mailo);
        m.cc(addresses);

        m.bcc("mailo1@anadollu.com", "Mail Mailo<mailo1@anadollu.com>");
        m.bcc("Mail Mailo<mailo1@anadollu.com>");
        m.bcc("mailo1@anadollu.com");
        m.bcc(mailo);
        m.bcc(addresses);

        m.replyTo("mailo1@anadollu.com", "Mail Mailo<mailo1@anadollu.com>");
        m.replyTo("Mail Mailo<mailo1@anadollu.com>");
        m.replyTo("mailo1@anadollu.com");
        m.replyTo(mailo);
        m.replyTo(addresses);
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidFromSimpleAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.from("woo foo @bar.com");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidFromFullAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.from("foo @bar.com", "Woo");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidToSimpleAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.to("woo foo @bar.com");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidToFullAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.to("foo @bar.com", "Woo");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidCcSimpleAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.cc("woo foo @bar.com");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidCcFullAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.cc("foo @bar.com", "Woo");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidBccSimpleAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.bcc("woo foo @bar.com");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidbccFullAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.bcc("foo @bar.com", "Woo");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidReplyToSimpleAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.replyTo("woo foo @bar.com");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidReplyToFullAddresses() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.replyTo("foo @bar.com", "Woo");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidDeliveryReceipt() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.deliveryReceipt("woo foo @bar.com");
    }

    @Test(expected = InvalidAddressException.class)
    public void invalidReadReceipt() {
        MailMessage m = new MailMessageImpl(MailoConfig.STANDART_CONFIG);

        m.readReceipt("woo foo @bar.com");
    }
}
