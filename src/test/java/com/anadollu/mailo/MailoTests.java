package com.anadollu.mailo;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.EmailMessage;
import com.anadollu.mailo.api.InvalidAddressException;
import com.anadollu.mailo.api.MessagePriority;
import com.anadollu.mailo.api.SendFailedException;
import com.anadollu.mailo.api.configuration.SessionConfig;
import com.anadollu.mailo.impl.MailMessageImpl;
import com.anadollu.mailo.impl.attachments.URLAttachment;
import com.anadollu.mailo.impl.util.MailoUtility;
import com.anadollu.mailo.impl.util.MessageConverter;
import com.anadollu.mailo.util.MailoConfig;
import com.anadollu.mailo.util.MailoTestUtil;

import org.junit.Assert;
import org.junit.Test;
import org.subethamail.wiser.Wiser;

import java.io.IOException;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class MailoTests {

    private static final String ENVELOPE_FROM_ADDRESS = "ef@anadollu.com";
    private static final String TEXT_MESSAGE_SUBJECT = "Text Message from Mail Mailo";
    private static final String HTML_MESSAGE_SUBJECT = "HTML Message from Mail Mailo";
    String fromName = "Mail Mailo";
    String fromAddress = "mailo@anadollu.com";
    String replyToName = "No Reply";
    String replyToAddress = "no-reply@anadollu.com";
    String toName = "Mail Mailor";
    String toAddress = "bookersportal@gmail.com";
    String ccName = "CCed Mailo";
    String ccAddress = "ccmailo@anadollu.com";
    String htmlBody = "<html><body><b>Hello</b> World!</body></html>";
    String textBody = "This is a Text Body!";
    String messageId = "message10090@anadollu.com";

    String createSubject(String val) {
        return val + " - " + java.util.UUID.randomUUID().toString();
    }

    @Test
    public void testTextMail() throws MessagingException, IOException {
        SessionConfig mailConfig = MailoConfig.STANDART_CONFIG;

        String subject = createSubject(TEXT_MESSAGE_SUBJECT);
        EmailMessage e;

        Wiser virtualSmtpServer = new Wiser(mailConfig.getServerPort());
        virtualSmtpServer.setHostname(mailConfig.getServerHost());
        try {
            virtualSmtpServer.start();

            e = new MailMessageImpl(mailConfig).from(MailoTestUtil.getAddressHeader(fromName, fromAddress))
                                               .replyTo(replyToAddress)
                                               .to(MailoTestUtil.getAddressHeader(toName, toAddress))
                                               .subject(subject)
                                               .bodyText(textBody)
                                               .importance(MessagePriority.HIGH)
                                               .messageId(messageId)
                                               .envelopeFrom(ENVELOPE_FROM_ADDRESS)
                                               .send();
        } finally {
            stop(virtualSmtpServer);
        }

        Assert.assertTrue("Didn't receive the expected amount of messages. Expected 1 got "
                + virtualSmtpServer.getMessages().size(), virtualSmtpServer.getMessages().size() == 1);

        MimeMessage mess = virtualSmtpServer.getMessages().get(0).getMimeMessage();

        Assert.assertEquals(MailoTestUtil.getAddressHeader(fromName, fromAddress), mess.getHeader("From", null));
        Assert.assertEquals(MailoTestUtil.getAddressHeader(replyToAddress), mess.getHeader("Reply-To", null));
        Assert.assertEquals(MailoTestUtil.getAddressHeader(toName, toAddress), mess.getHeader("To", null));
        Assert.assertEquals("Subject has been modified", subject, MimeUtility.unfold(mess.getHeader("Subject", null)));
        Assert.assertEquals(MessagePriority.HIGH.getPriority(), mess.getHeader("Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getX_priority(), mess.getHeader("X-Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getImportance(), mess.getHeader("Importance", null));
        Assert.assertTrue(mess.getHeader("Content-Type", null).startsWith("multipart/mixed"));
        Assert.assertEquals(messageId, MailoUtility.headerStripper(mess.getHeader("Message-ID", null)));

        MimeMultipart mixed = (MimeMultipart) mess.getContent();
        BodyPart text = mixed.getBodyPart(0);

        Assert.assertTrue(mixed.getContentType().startsWith("multipart/mixed"));
        Assert.assertEquals(1, mixed.getCount());

        Assert.assertTrue("Incorrect Charset: " + e.getCharset(),
                text.getContentType().startsWith("text/plain; charset=" + e.getCharset()));
        Assert.assertEquals(textBody, MailoTestUtil.getStringContent(text));
        EmailMessage convertedMessage = MessageConverter.convert(mess);
        Assert.assertEquals(convertedMessage.getSubject(), subject);
    }

    @Test
    public void testTextMailSpecialCharacters() throws MessagingException, IOException {
        SessionConfig mailConfig = MailoConfig.STANDART_CONFIG;

        String subject = createSubject("Sometimes subjects have speical characters like ü");
        String specialTextBody = "This is a Text Body with a special character - ü";

        EmailMessage e;

        Wiser wiser = new Wiser(mailConfig.getServerPort());
        wiser.setHostname(mailConfig.getServerHost());
        try {
            wiser.start();

            e =
                    new MailMessageImpl(mailConfig).from(MailoTestUtil.getAddressHeader(fromName, fromAddress))
                                                   .replyTo(replyToAddress)
                                                   .to(MailoTestUtil.getAddressHeader(toName, toAddress))
                                                   .subject(
                                                           subject)
                                                   .bodyText(specialTextBody)
                                                   .importance(MessagePriority.HIGH)
                                                   .messageId(messageId)
                                                   .send();
        } finally {
            stop(wiser);
        }

        Assert.assertTrue("Didn't receive the expected amount of messages. Expected 1 got "
                + wiser.getMessages().size(), wiser.getMessages().size() == 1);

        MimeMessage mess = wiser.getMessages().get(0).getMimeMessage();

        Assert.assertEquals("Subject has been modified", subject,
                MimeUtility.decodeText(MimeUtility.unfold(mess.getHeader("Subject", null))));

        MimeMultipart mixed = (MimeMultipart) mess.getContent();
        BodyPart text = mixed.getBodyPart(0);

        Assert.assertTrue(mixed.getContentType().startsWith("multipart/mixed"));
        Assert.assertEquals(1, mixed.getCount());

        Assert.assertTrue("Incorrect Charset: " + e.getCharset(),
                text.getContentType().startsWith("text/plain; charset=" + e.getCharset()));
        Assert.assertEquals(specialTextBody, MimeUtility.decodeText(MailoTestUtil.getStringContent(text)));
        EmailMessage convertedMessage = MessageConverter.convert(mess);
        Assert.assertEquals(convertedMessage.getSubject(), subject);
    }

    @Test
    public void testHTMLMail() throws MessagingException, IOException {
        SessionConfig mailConfig = MailoConfig.STANDART_CONFIG;

        String subject = createSubject(HTML_MESSAGE_SUBJECT);
        EmailContactTestImpl person = new EmailContactTestImpl(toName, toAddress);

        EmailMessage e;

        Wiser wiser = new Wiser(mailConfig.getServerPort());
        wiser.setHostname(mailConfig.getServerHost());
        try {
            wiser.start();
            e = new MailMessageImpl(mailConfig).from(MailoTestUtil.getAddressHeader(fromName, fromAddress))
                                               .replyTo(MailoTestUtil.getAddressHeader(replyToName, replyToAddress))
                                               .to(person)
                                               .subject(subject)
                                               .bodyHtml(htmlBody)
                                               .importance(MessagePriority.HIGH)
                                               .addAttachment(
                                                       new URLAttachment(
                                                               "http://office55.com/officeImages/OfficeLogo.png",
                                                               "office55Logo.png",
                                                               ContentDisposition.INLINE)
                                               ).messageId(subject).send();
        } finally {
            stop(wiser);
        }

        Assert.assertTrue("Didn't receive the expected amount of messages. Expected 1 got "
                + wiser.getMessages().size(), wiser.getMessages().size() == 1);

        MimeMessage mess = wiser.getMessages().get(0).getMimeMessage();

        Assert.assertEquals(MailoTestUtil.getAddressHeader(fromName, fromAddress), mess.getHeader("From", null));
        Assert.assertEquals(
                MailoTestUtil.getAddressHeader(replyToName, replyToAddress), mess.getHeader("Reply-To", null)
        );
        Assert.assertEquals(MailoTestUtil.getAddressHeader(toName, toAddress), mess.getHeader("To", null));
        Assert.assertEquals("Subject has been modified", subject, MimeUtility.unfold(mess.getHeader("Subject", null)));
        Assert.assertEquals(MessagePriority.HIGH.getPriority(), mess.getHeader("Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getX_priority(), mess.getHeader("X-Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getImportance(), mess.getHeader("Importance", null));
        Assert.assertEquals(e.getMessageId(), MailoUtility.headerStripper(mess.getHeader("Message-ID", null)));
        Assert.assertTrue(mess.getHeader("Content-Type", null).startsWith("multipart/mixed"));

        MimeMultipart mixed = (MimeMultipart) mess.getContent();
        MimeMultipart related = (MimeMultipart) mixed.getBodyPart(0).getContent();
        BodyPart html = related.getBodyPart(0);
        BodyPart attachment1 = related.getBodyPart(1);

        Assert.assertTrue(mixed.getContentType().startsWith("multipart/mixed"));
        Assert.assertEquals(1, mixed.getCount());

        Assert.assertTrue(related.getContentType().startsWith("multipart/related"));
        Assert.assertEquals(2, related.getCount());

        Assert.assertTrue(html.getContentType().startsWith("text/html"));
        Assert.assertEquals(htmlBody, MailoTestUtil.getStringContent(html));

        Assert.assertTrue(attachment1.getContentType().startsWith("image/png;"));
        Assert.assertEquals("office55Logo.png", attachment1.getFileName());
        EmailMessage convertedMessage = MessageConverter.convert(mess);
        Assert.assertEquals(convertedMessage.getSubject(), subject);
    }

    @Test(expected = SendFailedException.class)
    public void testTextMailSendFailed() {
        SessionConfig mailConfig = MailoConfig.STANDART_CONFIG;

        String subject = createSubject(TEXT_MESSAGE_SUBJECT);

        // Port is one off so this should fail
        Wiser wiser = new Wiser(mailConfig.getServerPort() + 1);
        wiser.setHostname(mailConfig.getServerHost());

        try {
            wiser.start();
            new MailMessageImpl(mailConfig).from(MailoTestUtil.getAddressHeader(fromName, fromAddress))
                                           .replyTo(replyToAddress)
                                           .to(toAddress)
                                           .subject(subject)
                                           .bodyText(textBody)
                                           .importance(
                                                   MessagePriority.HIGH)
                                           .messageId(messageId)
                                           .send();
        } finally {
            stop(wiser);
        }
    }

    @Test(expected = InvalidAddressException.class)
    public void testTextMailInvalidAddress() throws SendFailedException {
        SessionConfig mailConfig = MailoConfig.STANDART_CONFIG;

        String subject = createSubject(TEXT_MESSAGE_SUBJECT);

        // Port is one off so this should fail
        Wiser wiser = new Wiser(mailConfig.getServerPort() + 1);
        wiser.setHostname(mailConfig.getServerHost());

        try {
            wiser.start();
            new MailMessageImpl(mailConfig).from("mailo mailo@anadollu.com", fromName).replyTo(replyToAddress)
                                           .to(toAddress, toName).subject(subject).bodyText(textBody)
                                           .importance(MessagePriority.HIGH).messageId(messageId).send();
        } finally {
            stop(wiser);
        }
    }

    @Test
    public void testTextMailUsingPerson() throws MessagingException, IOException {
        SessionConfig mailConfig = MailoConfig.STANDART_CONFIG;

        String subject = createSubject(TEXT_MESSAGE_SUBJECT);
        EmailContactTestImpl person = new EmailContactTestImpl(toName, toAddress);

        EmailMessage e;

        Wiser wiser = new Wiser(mailConfig.getServerPort());
        wiser.setHostname(mailConfig.getServerHost());
        try {
            wiser.start();

            e = new MailMessageImpl(mailConfig).from(MailoTestUtil.getAddressHeader(fromName, fromAddress))
                                               .replyTo(replyToAddress)
                                               .to(person)
                                               .subject(subject)
                                               .bodyText(textBody)
                                               .importance(MessagePriority.HIGH)
                                               .messageId(messageId)
                                               .send();
        } finally {
            stop(wiser);
        }

        Assert.assertTrue("Didn't receive the expected amount of messages. Expected 1 got "
                + wiser.getMessages().size(), wiser.getMessages().size() == 1);

        MimeMessage mess = wiser.getMessages().get(0).getMimeMessage();

        Assert.assertEquals(MailoTestUtil.getAddressHeader(fromName, fromAddress), mess.getHeader("From", null));
        Assert.assertEquals(MailoTestUtil.getAddressHeader(replyToAddress), mess.getHeader("Reply-To", null));
        Assert.assertEquals(MailoTestUtil.getAddressHeader(toName, toAddress), mess.getHeader("To", null));
        Assert.assertEquals("Subject has been modified", subject, MimeUtility.unfold(mess.getHeader("Subject", null)));
        Assert.assertEquals(MessagePriority.HIGH.getPriority(), mess.getHeader("Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getX_priority(), mess.getHeader("X-Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getImportance(), mess.getHeader("Importance", null));
        Assert.assertTrue(mess.getHeader("Content-Type", null).startsWith("multipart/mixed"));
        Assert.assertEquals(messageId, MailoUtility.headerStripper(mess.getHeader("Message-ID", null)));

        MimeMultipart mixed = (MimeMultipart) mess.getContent();
        BodyPart text = mixed.getBodyPart(0);

        Assert.assertTrue(mixed.getContentType().startsWith("multipart/mixed"));
        Assert.assertEquals(1, mixed.getCount());

        Assert.assertTrue("Incorrect Charset: " + e.getCharset(),
                text.getContentType().startsWith("text/plain; charset=" + e.getCharset()));
        Assert.assertEquals(textBody, MailoTestUtil.getStringContent(text));
        EmailMessage convertedMessage = MessageConverter.convert(mess);
        Assert.assertEquals(convertedMessage.getSubject(), subject);
    }

    @Test
    public void testTextMailUsingDefaultSession() throws MessagingException, IOException {
        SessionConfig mailConfig = MailoConfig.STANDART_CONFIG;
        EmailContactTestImpl person = new EmailContactTestImpl(toName, toAddress);
        String subject = createSubject(TEXT_MESSAGE_SUBJECT);

        EmailMessage e;

        Wiser wiser = new Wiser(mailConfig.getServerPort());
        wiser.setHostname(mailConfig.getServerHost());
        try {
            wiser.start();
            e = new MailMessageImpl(mailConfig).from(MailoTestUtil.getAddressHeader(fromName, fromAddress))
                                               .replyTo(replyToAddress)
                                               .to(person)
                                               .subject(subject)
                                               .bodyText(textBody)
                                               .importance(MessagePriority.HIGH)
                                               .messageId(messageId)
                                               .send();
        } finally {
            stop(wiser);
        }

        Assert.assertTrue("Didn't receive the expected amount of messages. Expected 1 got "
                + wiser.getMessages().size(), wiser.getMessages().size() == 1);

        MimeMessage mess = wiser.getMessages().get(0).getMimeMessage();

        Assert.assertEquals(MailoTestUtil.getAddressHeader(fromName, fromAddress), mess.getHeader("From", null));
        Assert.assertEquals(MailoTestUtil.getAddressHeader(replyToAddress), mess.getHeader("Reply-To", null));
        Assert.assertEquals(MailoTestUtil.getAddressHeader(toName, toAddress), mess.getHeader("To", null));
        Assert.assertEquals("Subject has been modified", subject, MimeUtility.unfold(mess.getHeader("Subject", null)));
        Assert.assertEquals(MessagePriority.HIGH.getPriority(), mess.getHeader("Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getX_priority(), mess.getHeader("X-Priority", null));
        Assert.assertEquals(MessagePriority.HIGH.getImportance(), mess.getHeader("Importance", null));
        Assert.assertTrue(mess.getHeader("Content-Type", null).startsWith("multipart/mixed"));
        Assert.assertEquals(messageId, MailoUtility.headerStripper(mess.getHeader("Message-ID", null)));

        MimeMultipart mixed = (MimeMultipart) mess.getContent();
        BodyPart text = mixed.getBodyPart(0);

        Assert.assertTrue(mixed.getContentType().startsWith("multipart/mixed"));
        Assert.assertEquals(1, mixed.getCount());

        Assert.assertTrue("Incorrect Charset: " + e.getCharset(),
                text.getContentType().startsWith("text/plain; charset=" + e.getCharset()));
        Assert.assertEquals(textBody, MailoTestUtil.getStringContent(text));
        EmailMessage convertedMessage = MessageConverter.convert(mess);
        Assert.assertEquals(convertedMessage.getSubject(), subject);
    }

    protected void stop(Wiser wiser) {
        wiser.stop();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
