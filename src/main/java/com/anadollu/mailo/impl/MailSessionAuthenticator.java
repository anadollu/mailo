package com.anadollu.mailo.impl;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class MailSessionAuthenticator extends Authenticator {
    private String username;
    private String password;

    public MailSessionAuthenticator(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }
}
