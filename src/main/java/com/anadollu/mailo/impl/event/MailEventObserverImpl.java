package com.anadollu.mailo.impl.event;

import com.anadollu.mailo.api.MailMessage;
import com.anadollu.mailo.api.configuration.SessionConfig;
import com.anadollu.mailo.api.event.MailEvent;
import com.anadollu.mailo.api.event.MailEventObserver;
import com.anadollu.mailo.impl.BasicEmailContact;
import com.anadollu.mailo.impl.MailMessageImpl;

import javax.enterprise.inject.Vetoed;
import javax.inject.Inject;

@Vetoed
public class MailEventObserverImpl implements MailEventObserver {

    @Inject
    SessionConfig sessionConfig;

    @Override
    public void sendEmail(MailEvent event) {

        MailMessage mailMessage = new MailMessageImpl(sessionConfig);
        mailMessage.to(new BasicEmailContact(event.getTo(), event.getToName()))
                   .subject(event.getSubject())
                   .bodyHtml(event.getBody())
                   .addAttachments(event.getAttachments())
                   .send();

    }

}
