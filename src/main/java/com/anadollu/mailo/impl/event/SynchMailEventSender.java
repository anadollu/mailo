package com.anadollu.mailo.impl.event;

import com.anadollu.mailo.api.event.AsynchSend;
import com.anadollu.mailo.api.event.MailEvent;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;

@Stateless
public class SynchMailEventSender extends MailEventObserverImpl {

    @Asynchronous
    public void sendEmail(@Observes @AsynchSend MailEvent event) {
        super.sendEmail(event);
    }

}
