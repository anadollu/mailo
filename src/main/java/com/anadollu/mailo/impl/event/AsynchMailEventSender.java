package com.anadollu.mailo.impl.event;

import com.anadollu.mailo.api.event.MailEvent;
import com.anadollu.mailo.api.event.SynchSend;

import javax.enterprise.event.Observes;

public class AsynchMailEventSender extends MailEventObserverImpl {

    public void sendEmail(@Observes @SynchSend MailEvent event) {
        super.sendEmail(event);
    }

}
