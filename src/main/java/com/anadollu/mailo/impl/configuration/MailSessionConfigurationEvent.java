package com.anadollu.mailo.impl.configuration;

import com.anadollu.mailo.api.configuration.SessionConfigBuilder;

public class MailSessionConfigurationEvent {

    private SessionConfigBuilder builder;

    public MailSessionConfigurationEvent(SessionConfigBuilder builder) {
        super();
        this.builder = builder;
    }

    public SessionConfigBuilder getBuilder() {
        return builder;
    }

}
