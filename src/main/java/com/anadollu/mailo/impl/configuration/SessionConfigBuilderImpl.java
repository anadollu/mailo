package com.anadollu.mailo.impl.configuration;

import com.anadollu.mailo.api.configuration.SessionConfig;
import com.anadollu.mailo.api.configuration.SessionConfigBuilder;
import com.anadollu.mailo.impl.BasicEmailContact;

import java.util.Properties;

import static com.anadollu.mailo.api.configuration.SessionConfig.AUTH;
import static com.anadollu.mailo.api.configuration.SessionConfig.DOMAIN_NAME;
import static com.anadollu.mailo.api.configuration.SessionConfig.ENABLE_SSL;
import static com.anadollu.mailo.api.configuration.SessionConfig.ENABLE_TLS;
import static com.anadollu.mailo.api.configuration.SessionConfig.FROM;
import static com.anadollu.mailo.api.configuration.SessionConfig.FROM_NAME;
import static com.anadollu.mailo.api.configuration.SessionConfig.JNDI_SESSION_NAME;
import static com.anadollu.mailo.api.configuration.SessionConfig.PASSWORD;
import static com.anadollu.mailo.api.configuration.SessionConfig.REPLY_TO;
import static com.anadollu.mailo.api.configuration.SessionConfig.REPLY_TO_NAME;
import static com.anadollu.mailo.api.configuration.SessionConfig.REQUIRE_TLS;
import static com.anadollu.mailo.api.configuration.SessionConfig.SERVER_HOST;
import static com.anadollu.mailo.api.configuration.SessionConfig.SERVER_PORT;
import static com.anadollu.mailo.api.configuration.SessionConfig.USERNAME;

public class SessionConfigBuilderImpl implements SessionConfigBuilder {

    private Properties properties;

    public SessionConfigBuilderImpl() {
        this(null);
    }

    public SessionConfigBuilderImpl(Properties properties) {
        super();
        this.properties = properties != null ? properties : new Properties();
    }

    @Override
    public SessionConfigBuilder serverHost(String serverHost) {
        return set(SERVER_HOST, serverHost);
    }

    @Override
    public SessionConfigBuilder serverPort(int serverPort) {
        return set(SERVER_PORT, serverPort);
    }

    @Override
    public SessionConfigBuilder domainName(String domainName) {
        return set(DOMAIN_NAME, domainName);
    }

    @Override
    public SessionConfigBuilder username(String username) {
        return set(USERNAME, username);
    }

    @Override
    public SessionConfigBuilder password(String password) {
        return set(PASSWORD, password);
    }

    @Override
    public SessionConfigBuilder enableTls(boolean enableTls) {
        return set(ENABLE_TLS, enableTls);
    }

    @Override
    public SessionConfigBuilder requireTls(boolean requireTls) {
        return set(REQUIRE_TLS, requireTls);
    }

    @Override
    public SessionConfigBuilder enableSsl(boolean enableSsl) {
        return set(ENABLE_SSL, enableSsl);
    }

    @Override
    public SessionConfigBuilder auth(boolean auth) {
        return set(AUTH, auth);
    }

    @Override
    public SessionConfigBuilder jndiSessionName(String jndiSessionName) {
        return set(JNDI_SESSION_NAME, jndiSessionName);
    }

    public SessionConfigBuilder from(String from) {
        return set(FROM, from);
    }

    public SessionConfigBuilder fromName(String fromName) {
        return set(FROM_NAME, fromName);
    }

    public SessionConfigBuilder replyTo(String replyTo) {
        return set(REPLY_TO, replyTo);
    }

    public SessionConfigBuilder replyToName(String replyToName) {
        return set(REPLY_TO_NAME, replyToName);
    }

    private SessionConfigBuilder set(String name, Object value) {
        properties.put(name, value);
        return this;
    }

    @Override
    public SessionConfig build() {

        SessionConfigImpl simpleMailConfig = new SessionConfigImpl();
        simpleMailConfig.setDomainName(stringValue(DOMAIN_NAME));
        simpleMailConfig.setJndiSessionName(stringValue(JNDI_SESSION_NAME));
        simpleMailConfig.setServerHost(stringValue(SERVER_HOST));
        simpleMailConfig.setServerPort(intValue(SERVER_PORT));
        simpleMailConfig.setUsername(stringValue(USERNAME));
        simpleMailConfig.setPassword(stringValue(PASSWORD));
        simpleMailConfig.setEnableSsl(booleanValue(ENABLE_SSL));
        simpleMailConfig.setRequireTls(booleanValue(REQUIRE_TLS));
        simpleMailConfig.setEnableTls(booleanValue(ENABLE_TLS));
        simpleMailConfig.setAuth(booleanValue(AUTH));

        BasicEmailContact from = new BasicEmailContact(stringValue(FROM), stringValue(FROM_NAME));
        BasicEmailContact replyTo = new BasicEmailContact(stringValue(REPLY_TO), stringValue(REPLY_TO_NAME));

        simpleMailConfig.setDefaultSender(from);
        simpleMailConfig.setDefaultReplyTo(replyTo);

        return simpleMailConfig;

    }

    private String stringValue(String key) {
        return properties.getProperty(key);
    }

    private int intValue(String key) {
        Object value = properties.get(key);
        if (value instanceof Integer) {
            return (Integer) value;
        } else if (value instanceof String) {
            try {
                return Integer.valueOf((String) value);
            } catch (Exception ignore) {
            }
        }
        return 0;
    }

    private boolean booleanValue(String key) {
        Object value = properties.get(key);
        if (value instanceof Boolean) {
            return (Boolean) value;
        } else if (value instanceof String) {
            try {
                return Boolean.parseBoolean((String) value);
            } catch (Exception ignore) {
            }
        }
        return false;
    }

}
