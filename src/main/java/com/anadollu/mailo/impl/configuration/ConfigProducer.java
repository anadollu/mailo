package com.anadollu.mailo.impl.configuration;

import com.anadollu.mailo.api.configuration.SessionConfig;
import com.anadollu.mailo.extension.MailoExtension;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;

public class ConfigProducer {

    @Inject
    MailoExtension extension;

    @Produces
    public SessionConfig getDefaultSessionConfig() {
        return extension.getSessionConfig();
    }

}
