package com.anadollu.mailo.impl;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.ContentType;
import com.anadollu.mailo.api.EmailContact;
import com.anadollu.mailo.api.EmailMessage;
import com.anadollu.mailo.api.EmailMessageType;
import com.anadollu.mailo.api.Header;
import com.anadollu.mailo.api.ICalMethod;
import com.anadollu.mailo.api.MailMessage;
import com.anadollu.mailo.api.MailTransporter;
import com.anadollu.mailo.api.MessagePriority;
import com.anadollu.mailo.api.SendFailedException;
import com.anadollu.mailo.api.attachment.EmailAttachment;
import com.anadollu.mailo.api.configuration.SessionConfig;
import com.anadollu.mailo.impl.attachments.BaseAttachment;
import com.anadollu.mailo.impl.attachments.FileAttachment;
import com.anadollu.mailo.impl.attachments.InputStreamAttachment;
import com.anadollu.mailo.impl.util.MailoUtility;
import com.anadollu.mailo.impl.util.Strings;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;

import javax.enterprise.inject.Vetoed;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
@Vetoed
public class MailMessageImpl implements MailMessage {

    private EmailMessage emailMessage;

    private MailTransporter mailTransporter;
    private Session session;
    private SessionConfig mailConfig;

    private MailMessageImpl() {
        emailMessage = new EmailMessage();

    }

    public MailMessageImpl(Session session) {
        this();
        this.session = session;
    }

    public MailMessageImpl(MailTransporter mailTransporter) {
        this();
        this.mailTransporter = mailTransporter;
    }

    public MailMessageImpl(SessionConfig mailConfig) {
        this();
        this.mailConfig = mailConfig;
        from(mailConfig.getDefaultSender());
        replyTo(mailConfig.getDefaultReplyTo());
    }

    // Begin Addressing

    public MailMessage from(String... address) {
        emailMessage.addFromAddresses(MailoUtility.internetAddress(address));
        return this;
    }

    public MailMessage from(InternetAddress emailAddress) {
        emailMessage.addFromAddress(emailAddress);
        return this;
    }

    public MailMessage from(EmailContact emailContact) {
        if (!isEmptyContact(emailContact)) {
            emailMessage.addFromAddress(MailoUtility.internetAddress(emailContact));
        }
        return this;
    }

    public MailMessage from(Collection<? extends EmailContact> emailContacts) {
        emailMessage.addFromAddresses(MailoUtility.internetAddress(emailContacts));
        return this;
    }

    public MailMessage replyTo(String... address) {
        emailMessage.addReplyToAddresses(MailoUtility.internetAddress(address));
        return this;
    }

    public MailMessage replyTo(InternetAddress emailAddress) {
        emailMessage.addReplyToAddress(emailAddress);
        return this;
    }

    public MailMessage replyTo(EmailContact emailContact) {
        if (!isEmptyContact(emailContact)) {
            emailMessage.addReplyToAddress(MailoUtility.internetAddress(emailContact));
        }
        return this;
    }

    public MailMessage replyTo(Collection<? extends EmailContact> emailContacts) {
        emailMessage.addReplyToAddresses(MailoUtility.internetAddress(emailContacts));
        return this;
    }

    public MailMessage addHeader(String name, String value) {
        emailMessage.addHeader(new Header(name, value));
        return this;
    }

    public MailMessage to(String... address) {
        emailMessage.addToAddresses(MailoUtility.internetAddress(address));
        return this;
    }

    public MailMessage to(InternetAddress emailAddress) {
        emailMessage.addToAddress(emailAddress);
        return this;
    }

    public MailMessage to(EmailContact emailContact) {
        if (emailContact != null) {
            emailMessage.addToAddress(MailoUtility.internetAddress(emailContact));
        }
        return this;
    }

    public MailMessage to(Collection<? extends EmailContact> emailContacts) {
        emailMessage.addToAddresses(MailoUtility.internetAddress(emailContacts));
        return this;
    }

    public MailMessage cc(String... address) {
        emailMessage.addCcAddresses(MailoUtility.internetAddress(address));
        return this;
    }

    public MailMessage cc(InternetAddress emailAddress) {
        emailMessage.addCcAddress(emailAddress);
        return this;
    }

    public MailMessage cc(EmailContact emailContact) {
        if (!isEmptyContact(emailContact)) {
            emailMessage.addCcAddress(MailoUtility.internetAddress(emailContact));
        }
        return this;
    }

    public MailMessage cc(Collection<? extends EmailContact> emailContacts) {
        emailMessage.addCcAddresses(MailoUtility.internetAddress(emailContacts));
        return this;
    }

    public MailMessage bcc(String... address) {
        emailMessage.addBccAddresses(MailoUtility.internetAddress(address));
        return this;
    }

    public MailMessage bcc(InternetAddress emailAddress) {
        emailMessage.addBccAddress(emailAddress);
        return this;
    }

    public MailMessage bcc(EmailContact emailContact) {
        if (!isEmptyContact(emailContact)) {
            emailMessage.addBccAddress(MailoUtility.internetAddress(emailContact));
        }
        return this;
    }

    public MailMessage bcc(Collection<? extends EmailContact> emailContacts) {
        emailMessage.addBccAddresses(MailoUtility.internetAddress(emailContacts));
        return this;
    }

    public MailMessage envelopeFrom(EmailContact emailContact) {
        if (!isEmptyContact(emailContact)) {
            emailMessage.setEnvelopeFrom(MailoUtility.internetAddress(emailContact));
        }
        return this;
    }

    public MailMessage envelopeFrom(String address) {
        if (address != null) {
            emailMessage.setEnvelopeFrom(MailoUtility.internetAddress(address));
        }
        return this;
    }

    // End Addressing

    public MailMessage subject(String value) {
        emailMessage.setSubject(value);
        return this;
    }

    public MailMessage deliveryReceipt(String address) {
        emailMessage.addDeliveryReceiptAddress(MailoUtility.internetAddress(address));
        return this;
    }

    public MailMessage readReceipt(String address) {
        emailMessage.addReadReceiptAddress(MailoUtility.internetAddress(address));
        return this;
    }

    public MailMessage importance(MessagePriority messagePriority) {
        emailMessage.setImportance(messagePriority);
        return this;
    }

    public MailMessage messageId(String messageId) {
        emailMessage.setMessageId(messageId);
        return this;
    }

    public MailMessage bodyText(String text) {
        emailMessage.setTextBody(text);
        return this;
    }

    public MailMessage bodyHtml(String html) {
        emailMessage.setHtmlBody(html);
        return this;
    }

    public MailMessage bodyHtmlTextAlt(String html, String text) {
        emailMessage.setTextBody(text);
        emailMessage.setHtmlBody(html);
        return this;
    }

    // Begin Attachments

    public MailMessage addAttachment(EmailAttachment attachment) {
        emailMessage.addAttachment(attachment);
        return this;
    }

    public MailMessage addAttachments(Collection<? extends EmailAttachment> attachments) {
        emailMessage.addAttachments(attachments);
        return this;
    }

    public MailMessage addAttachment(String fileName, String mimeType, ContentDisposition contentDispostion,
                                     byte[] bytes) {
        addAttachment(new BaseAttachment(fileName, mimeType, contentDispostion, bytes));
        return this;
    }

    public MailMessage addAttachment(String fileName, String mimeType, ContentDisposition contentDispostion,
                                     InputStream inputStream) {
        addAttachment(new InputStreamAttachment(fileName, mimeType, contentDispostion, inputStream));
        return this;
    }

    public MailMessage addAttachment(ContentDisposition contentDispostion, File file) {
        addAttachment(new FileAttachment(contentDispostion, file));
        return this;
    }

    // End Attachments

    // Begin Calendar

    public MailMessage iCal(String htmlBody, String textBody, ICalMethod method, byte[] bytes) {
        emailMessage.setType(EmailMessageType.INVITE_ICAL);
        emailMessage.setHtmlBody(htmlBody);
        emailMessage.setTextBody(textBody);
        emailMessage.addAttachment(new BaseAttachment(null, "text/calendar;method=" + method,
                ContentDisposition.INLINE, bytes, "urn:content-classes:calendarmessage"));
        return this;
    }

    public MailMessage iCal(String textBody, ICalMethod method, byte[] bytes) {
        emailMessage.setType(EmailMessageType.INVITE_ICAL);
        emailMessage.setTextBody(textBody);
        emailMessage.addAttachment(new BaseAttachment(null, "text/calendar;method=" + method,
                ContentDisposition.INLINE, bytes, "urn:content-classes:calendarmessage"));
        return this;
    }

    // End Calendar

    public MailMessage charset(String charset) {
        emailMessage.setCharset(charset);
        return this;
    }

    public MailMessage contentType(ContentType contentType) {
        emailMessage.setRootContentType(contentType);
        return this;
    }

    public EmailMessage getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(EmailMessage emailMessage) {
        this.emailMessage = emailMessage;
    }

    public void setMailTransporter(MailTransporter mailTransporter) {
        this.mailTransporter = mailTransporter;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    boolean isEmptyContact(EmailContact emailContact) {
        return emailContact == null
                || (Strings.isNullOrBlank(emailContact.getName()) && Strings.isNullOrBlank(emailContact.getAddress()));
    }

    public EmailMessage send(MailTransporter mailTransporter) throws SendFailedException {

        try {
            mailTransporter.send(emailMessage);
        } catch (Exception e) {
            throw new SendFailedException("Send Failed", e);
        }

        return emailMessage;
    }

    private EmailMessage send(Session session) throws SendFailedException {
        return send(new MailTransporterImpl(session));
    }

    public EmailMessage send(SessionConfig mailConfig) {
        if (emailMessage.isSenderEmpty())
            from(mailConfig.getDefaultSender());

        if (emailMessage.isReplyToEmpty())
            replyTo(mailConfig.getDefaultReplyTo());

        return send(MailoUtility.createSession(mailConfig));
    }

    public EmailMessage send() throws SendFailedException {
        if (mailTransporter != null) {
            return send(mailTransporter);
        } else if (session != null) {
            return send(session);
        } else if (mailConfig != null) {
            return send(mailConfig);
        } else {
            throw new SendFailedException("No Resource availiable to send. How was this constructed?");
        }
    }

}
