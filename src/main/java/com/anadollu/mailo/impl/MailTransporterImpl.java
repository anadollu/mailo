package com.anadollu.mailo.impl;

import com.anadollu.mailo.api.EmailMessage;
import com.anadollu.mailo.api.MailTransporter;
import com.anadollu.mailo.impl.util.MailoUtility;

import javax.mail.Session;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class MailTransporterImpl implements MailTransporter {

    private Session session;

    public MailTransporterImpl(Session session) {
        this.session = session;
    }

    public EmailMessage send(EmailMessage emailMessage) {
        MailoUtility.send(emailMessage, session);
        return emailMessage;
    }

}
