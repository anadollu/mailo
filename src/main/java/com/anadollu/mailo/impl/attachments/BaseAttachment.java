package com.anadollu.mailo.impl.attachments;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.Header;
import com.anadollu.mailo.api.attachment.EmailAttachment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class BaseAttachment implements EmailAttachment {
    private String contentId;
    private String fileName;
    private String mimeType;
    private ContentDisposition contentDisposition;
    private Collection<Header> headers = new ArrayList<Header>();
    private byte[] bytes;

    public BaseAttachment(String fileName, String mimeType, ContentDisposition contentDisposition, byte[] bytes) {
        this();
        this.fileName = fileName;
        this.mimeType = mimeType;
        this.contentDisposition = contentDisposition;
        this.bytes = bytes;
    }

    public BaseAttachment(String fileName, String mimeType, ContentDisposition contentDisposition, byte[] bytes,
                          String contentClass) {
        this(fileName, mimeType, contentDisposition, bytes);
        this.addHeader(new Header("Content-Class", contentClass));
    }

    public BaseAttachment() {
        this.contentId = UUID.randomUUID().toString();
    }

    public String getContentId() {
        return contentId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public ContentDisposition getContentDisposition() {
        return contentDisposition;
    }

    public void setContentDisposition(ContentDisposition contentDisposition) {
        this.contentDisposition = contentDisposition;
    }

    public Collection<Header> getHeaders() {
        return headers;
    }

    public void addHeader(Header header) {
        headers.add(header);
    }

    public void addHeaders(Collection<Header> headers) {
        headers.addAll(headers);
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
