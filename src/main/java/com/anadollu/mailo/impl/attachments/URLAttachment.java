package com.anadollu.mailo.impl.attachments;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.Header;
import com.anadollu.mailo.api.attachment.AttachmentException;
import com.anadollu.mailo.impl.util.Streams;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.activation.URLDataSource;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class URLAttachment extends BaseAttachment {
    public URLAttachment(String url, String fileName, ContentDisposition contentDisposition) {
        super();
        URLDataSource uds;
        try {
            uds = new URLDataSource(new URL(url));
            super.setFileName(fileName);
            super.setMimeType(uds.getContentType());
            super.setContentDisposition(contentDisposition);
            super.setBytes(Streams.toByteArray(uds.getInputStream()));
        } catch (MalformedURLException e) {
            throw new AttachmentException("Wasn't able to create email attachment from URL: " + url, e);
        } catch (IOException e) {
            throw new AttachmentException("Wasn't able to create email attachment from URL: " + url, e);
        }
    }

    public URLAttachment(String url, String fileName, ContentDisposition contentDisposition, String contentClass) {
        this(url, fileName, contentDisposition);
        super.addHeader(new Header("Content-Class", contentClass));
    }
}
