package com.anadollu.mailo.impl.attachments;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.Header;
import com.anadollu.mailo.api.attachment.AttachmentException;
import com.anadollu.mailo.impl.util.Streams;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class InputStreamAttachment extends BaseAttachment {
    public InputStreamAttachment(String fileName, String mimeType, ContentDisposition contentDisposition,
                                 InputStream inputStream) {
        super();

        try {
            super.setFileName(fileName);
            super.setMimeType(mimeType);
            super.setContentDisposition(contentDisposition);
            super.setBytes(Streams.toByteArray(inputStream));
        } catch (IOException e) {
            throw new AttachmentException("Wasn't able to create email attachment from InputStream");
        }
    }

    public InputStreamAttachment(String fileName, String mimeType, ContentDisposition contentDisposition,
                                 InputStream inputStream, String contentClass) {
        this(fileName, mimeType, contentDisposition, inputStream);
        super.addHeader(new Header("Content-Class", contentClass));
    }
}
