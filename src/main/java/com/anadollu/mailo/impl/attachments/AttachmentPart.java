package com.anadollu.mailo.impl.attachments;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.Header;

import java.util.ArrayList;
import java.util.Collection;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class AttachmentPart extends MimeBodyPart {

    private String uid;

    public AttachmentPart(DataSource dataSource, String uid, String fileName, Collection<Header> headers,
                          ContentDisposition contentDisposition) {
        super();

        this.uid = uid;

        try {
            setContentID("<" + uid + ">");
        } catch (MessagingException e1) {
            throw new RuntimeException("Unable to set unique content-id on attachment");
        }

        setData(dataSource);

        if (fileName != null) {
            try {
                setFileName(fileName);
            } catch (MessagingException e) {
                throw new RuntimeException("Unable to get FileName on attachment");
            }
        }

        if (headers != null) {
            for (Header header : headers) {
                try {
                    addHeader(header.getName(), header.getValue());

                } catch (MessagingException e) {
                    throw new RuntimeException("Unable to add Content-Class Header");
                }
            }
        }

        setContentDisposition(contentDisposition);
    }

    public AttachmentPart(byte[] bytes, String uid, String fileName, String mimeType, Collection<Header> headers,
                          ContentDisposition contentDisposition) {
        this(getByteArrayDataSource(bytes, mimeType), uid, fileName, headers, contentDisposition);
    }

    public AttachmentPart(byte[] bytes, String uid, String fileName, String mimeType,
                          ContentDisposition contentDisposition) {
        this(getByteArrayDataSource(bytes, mimeType), uid, fileName, new ArrayList<Header>(), contentDisposition);
    }

    private static ByteArrayDataSource getByteArrayDataSource(byte[] bytes, String mimeType) {
        ByteArrayDataSource bads = new ByteArrayDataSource(bytes, mimeType);
        return bads;
    }

    public String getAttachmentFileName() {
        try {
            return getFileName();
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to get File Name from attachment");
        }
    }

    public ContentDisposition getContentDisposition() {
        try {
            return ContentDisposition.mapValue(getDisposition());
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to get Content-Dispostion on attachment");
        }
    }

    public void setContentDisposition(ContentDisposition contentDisposition) {
        try {
            setDisposition(contentDisposition.headerValue());
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to set Content-Dispostion on attachment");
        }
    }

    public String getUid() {
        return uid;
    }

    private void setData(DataSource datasource) {
        try {
            setDataHandler(new DataHandler(datasource));
        } catch (MessagingException e) {
            throw new RuntimeException("Unable to set Data on attachment");
        }
    }
}
