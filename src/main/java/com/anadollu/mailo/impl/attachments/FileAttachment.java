package com.anadollu.mailo.impl.attachments;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.Header;
import com.anadollu.mailo.api.attachment.AttachmentException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class FileAttachment extends BaseAttachment {

    public FileAttachment(ContentDisposition contentDisposition, Path path) {
        super();
        try {
            super.setFileName(path.getFileName().toString());
            super.setMimeType(Files.probeContentType(path));
            super.setContentDisposition(contentDisposition);
            super.setBytes(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new AttachmentException("Wasn't able to create email attachment from File: "
                    + path.getFileName().toString(), e);
        }
    }

    public FileAttachment(ContentDisposition contentDisposition, Path path, String contentClass) {
        this(contentDisposition, path);
        super.addHeader(new Header("Content-Class", contentClass));
    }

    public FileAttachment(ContentDisposition contentDisposition, File file) {
        this(contentDisposition, file.toPath());
    }

    public FileAttachment(ContentDisposition contentDisposition, File file, String contentClass) {
        this(contentDisposition, file.toPath());
        super.addHeader(new Header("Content-Class", contentClass));
    }
}
