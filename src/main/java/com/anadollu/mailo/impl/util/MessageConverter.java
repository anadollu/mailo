package com.anadollu.mailo.impl.util;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.EmailMessage;
import com.anadollu.mailo.api.MailException;
import com.anadollu.mailo.impl.attachments.InputStreamAttachment;

import java.io.IOException;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class MessageConverter {

    private EmailMessage emailMessage;

    public static EmailMessage convert(Message m) throws MailException {
        MessageConverter mc = new MessageConverter();
        return mc.convertMessage(m);
    }

    public EmailMessage convertMessage(Message m) throws MailException {
        emailMessage = new EmailMessage();

        try {
            emailMessage.setFromAddresses(MailoUtility.getInternetAddressses(m.getFrom()));
            emailMessage.setToAddresses(MailoUtility.getInternetAddressses(m.getRecipients(RecipientType.TO)));
            emailMessage.setCcAddresses(MailoUtility.getInternetAddressses(m.getRecipients(RecipientType.CC)));
            emailMessage.setBccAddresses(MailoUtility.getInternetAddressses(m.getRecipients(RecipientType.BCC)));
            emailMessage.setSubject(m.getSubject());
            emailMessage.setMessageId(m.getHeader("Message-ID")[0]);
            emailMessage.addHeaders(MailoUtility.getHeaders(m.getAllHeaders()));

            if (m.getContentType().toLowerCase().contains("multipart/")) {
                addMultiPart((MimeMultipart) m.getContent());
            } else if (m.isMimeType("text/plain")) {
                emailMessage.setTextBody((String) m.getContent());
            }
        } catch (IOException e) {
            throw new MailException(e);
        } catch (MessagingException e) {
            throw new MailException(e);
        }

        return emailMessage;
    }

    private void addMultiPart(MimeMultipart mp) throws MessagingException, IOException {
        for (int i = 0; i < mp.getCount(); i++) {
            BodyPart bp = mp.getBodyPart(i);
            if (bp.getContentType().toLowerCase().contains("multipart/")) {
                addMultiPart((MimeMultipart) bp.getContent());
            } else {
                addPart(mp.getBodyPart(i));
            }
        }
    }

    private void addPart(BodyPart bp) throws MessagingException, IOException {

        if (bp.getContentType().toLowerCase().contains("multipart/")) {
            addMultiPart((MimeMultipart) bp.getContent());
        } else if (bp.getContentType().toLowerCase().contains("text/plain")) {
            emailMessage.setTextBody((String) bp.getContent());
        } else if (bp.getContentType().toLowerCase().contains("text/html")) {
            emailMessage.setHtmlBody((String) bp.getContent());
        } else if (bp.getContentType().toLowerCase().contains("application/octet-stream")) {
            emailMessage.addAttachment(new InputStreamAttachment(bp.getFileName(), bp.getContentType(),
                    ContentDisposition.mapValue(bp.getDisposition()), bp.getInputStream()));
        }
    }
}
