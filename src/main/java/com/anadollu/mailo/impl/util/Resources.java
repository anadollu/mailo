package com.anadollu.mailo.impl.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class Resources {

    private static final String UNDERSCORE = "_";

    public static InputStream openResource(String resourceName) {
        return Resources.class.getClassLoader().getResourceAsStream(resourceName);
    }

    public static String localizedResourceName(String resourceName, Locale locale) {

        if (locale == null)
            return resourceName;

        int idx = resourceName.lastIndexOf(".");

        if (idx > 0) {
            return resourceName.substring(0, idx - 1) + UNDERSCORE + locale.getLanguage() + resourceName.substring(idx);
        } else {
            return resourceName + UNDERSCORE + locale.getLanguage();
        }

    }

    public static final Properties readProperties(String resourceName) throws IOException {

        Properties properties = new Properties();
        InputStream inputStream = openResource(resourceName);
        if (inputStream != null) {
            properties.load(inputStream);
            inputStream.close();
        }
        return properties;

    }

    public static final ResourceBundle readMessagesBundle(String resourceName) {
        return readMessagesBundle(resourceName, Locale.getDefault());
    }

    public static final ResourceBundle readMessagesBundle(String resourceName, Locale locale) {
        ResourceBundle bundle = ResourceBundle.getBundle(resourceName, locale, Resources.class.getClassLoader());
        return bundle;
    }

}
