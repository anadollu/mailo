package com.anadollu.mailo.impl.util;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class Strings {
    /**
     * Checks if a String is null or empty when trimmed
     *
     * @return TRUE if NULL or string.trim.length == 0
     */
    public static boolean isNullOrBlank(String string) {
        return string == null || string.trim().length() == 0;
    }
}
