package com.anadollu.mailo.impl.util;

import com.anadollu.mailo.api.attachment.EmailAttachment;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class EmailAttachmentUtil {
    public static Map<String, EmailAttachment> getEmailAttachmentMap(Collection<EmailAttachment> attachments) {
        Map<String, EmailAttachment> emailAttachmentMap = new HashMap<String, EmailAttachment>();

        for (EmailAttachment ea : attachments) {
            if (!Strings.isNullOrBlank(ea.getFileName())) {
                emailAttachmentMap.put(ea.getFileName(), ea);
            }
        }

        return emailAttachmentMap;
    }

}
