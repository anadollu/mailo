package com.anadollu.mailo.impl;

import com.sun.mail.smtp.SMTPMessage;

import java.io.InputStream;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

/**
 * Extends {@link MimeMessage} to allow for the setting of the Message-ID
 *
 * @author @author Anatolian ( sertac at anadollu dot com)
 */
public class RootMimeMessage extends SMTPMessage {
    private String messageId;

    public RootMimeMessage(Session session) {
        super(session);
    }

    public RootMimeMessage(Session session, InputStream inputStream) throws MessagingException {
        super(session, inputStream);
    }

    @Override
    protected void updateMessageID() throws MessagingException {
        Header header = new Header("Message-ID", messageId);
        setHeader(header.getName(), header.getValue());
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }
}
