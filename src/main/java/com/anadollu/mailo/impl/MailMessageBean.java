package com.anadollu.mailo.impl;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.ContentType;
import com.anadollu.mailo.api.EmailContact;
import com.anadollu.mailo.api.EmailMessage;
import com.anadollu.mailo.api.ICalMethod;
import com.anadollu.mailo.api.MailMessage;
import com.anadollu.mailo.api.MessagePriority;
import com.anadollu.mailo.api.attachment.EmailAttachment;
import com.anadollu.mailo.api.configuration.SessionConfig;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.mail.internet.InternetAddress;

public class MailMessageBean implements MailMessage {

    @Inject
    SessionConfig sessionConfig;
    private MailMessage mailMessage;

    @PostConstruct
    protected void init() {
        this.mailMessage = new MailMessageImpl(sessionConfig);
    }

    public MailMessage from(String... address) {
        return mailMessage.from(address);
    }

    public MailMessage from(InternetAddress emailAddress) {
        return mailMessage.from(emailAddress);
    }

    public MailMessage from(EmailContact emailContact) {
        return mailMessage.from(emailContact);
    }

    public MailMessage from(Collection<? extends EmailContact> emailContacts) {
        return mailMessage.from(emailContacts);
    }

    public MailMessage replyTo(String... address) {
        return mailMessage.replyTo(address);
    }

    public MailMessage replyTo(InternetAddress emailAddress) {
        return mailMessage.replyTo(emailAddress);
    }

    public MailMessage replyTo(EmailContact emailContact) {
        return mailMessage.replyTo(emailContact);
    }

    public MailMessage replyTo(Collection<? extends EmailContact> emailContacts) {
        return mailMessage.replyTo(emailContacts);
    }

    public MailMessage addHeader(String name, String value) {
        return mailMessage.addHeader(name, value);
    }

    public MailMessage to(String... address) {
        return mailMessage.to(address);
    }

    public MailMessage to(InternetAddress emailAddress) {
        return mailMessage.to(emailAddress);
    }

    public MailMessage to(EmailContact emailContact) {
        return mailMessage.to(emailContact);
    }

    public MailMessage to(Collection<? extends EmailContact> emailContacts) {
        return mailMessage.to(emailContacts);
    }

    public MailMessage cc(String... address) {
        return mailMessage.cc(address);
    }

    public MailMessage cc(InternetAddress emailAddress) {
        return mailMessage.cc(emailAddress);
    }

    public MailMessage cc(EmailContact emailContact) {
        return mailMessage.cc(emailContact);
    }

    public MailMessage cc(Collection<? extends EmailContact> emailContacts) {
        return mailMessage.cc(emailContacts);
    }

    public MailMessage bcc(String... address) {
        return mailMessage.bcc(address);
    }

    public MailMessage bcc(InternetAddress emailAddress) {
        return mailMessage.bcc(emailAddress);
    }

    public MailMessage bcc(EmailContact emailContact) {
        return mailMessage.bcc(emailContact);
    }

    public MailMessage bcc(Collection<? extends EmailContact> emailContacts) {
        return mailMessage.bcc(emailContacts);
    }

    public MailMessage envelopeFrom(EmailContact emailContact) {
        return mailMessage.envelopeFrom(emailContact);
    }

    public MailMessage envelopeFrom(String address) {
        return mailMessage.envelopeFrom(address);
    }

    public MailMessage addAttachment(EmailAttachment attachment) {
        return mailMessage.addAttachment(attachment);
    }

    public MailMessage addAttachments(Collection<? extends EmailAttachment> attachments) {
        return mailMessage.addAttachments(attachments);
    }

    public MailMessage addAttachment(String fileName, String mimeType, ContentDisposition contentDispostion,
                                     byte[] bytes) {
        return mailMessage.addAttachment(fileName, mimeType, contentDispostion, bytes);
    }

    public MailMessage addAttachment(String fileName, String mimeType, ContentDisposition contentDispostion,
                                     InputStream inputStream) {
        return mailMessage.addAttachment(fileName, mimeType, contentDispostion, inputStream);
    }

    public MailMessage addAttachment(ContentDisposition contentDispostion, File file) {
        return mailMessage.addAttachment(contentDispostion, file);
    }

    public MailMessage importance(MessagePriority messagePriority) {
        return mailMessage.importance(messagePriority);
    }

    public MailMessage deliveryReceipt(String address) {
        return mailMessage.deliveryReceipt(address);
    }

    public MailMessage readReceipt(String address) {
        return mailMessage.readReceipt(address);
    }

    public MailMessage messageId(String messageId) {
        return mailMessage.messageId(messageId);
    }

    public MailMessage iCal(String textBody, ICalMethod method, byte[] bytes) {
        return mailMessage.iCal(textBody, method, bytes);
    }

    public MailMessage iCal(String htmlBody, String textBody, ICalMethod method, byte[] bytes) {
        return mailMessage.iCal(htmlBody, textBody, method, bytes);
    }

    public MailMessage subject(String value) {
        return mailMessage.subject(value);
    }

    public MailMessage bodyText(String text) {
        return mailMessage.bodyText(text);
    }

    public MailMessage bodyHtml(String html) {
        return mailMessage.bodyHtml(html);
    }

    public MailMessage bodyHtmlTextAlt(String html, String text) {
        return mailMessage.bodyHtmlTextAlt(html, text);
    }

    public MailMessage charset(String charset) {
        return mailMessage.charset(charset);
    }

    public MailMessage contentType(ContentType contentType) {
        return mailMessage.contentType(contentType);
    }

    public EmailMessage getEmailMessage() {
        return mailMessage.getEmailMessage();
    }

    public void setEmailMessage(EmailMessage emailMessage) {
        mailMessage.setEmailMessage(emailMessage);
    }

    public EmailMessage send() {
        return mailMessage.send();
    }

}
