package com.anadollu.mailo.extension;

import com.anadollu.mailo.api.configuration.SessionConfig;
import com.anadollu.mailo.api.configuration.SessionConfigBuilder;
import com.anadollu.mailo.impl.configuration.MailSessionConfigurationEvent;
import com.anadollu.mailo.impl.configuration.SessionConfigBuilderImpl;

import java.io.IOException;
import java.util.Properties;

import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.AfterDeploymentValidation;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.Extension;

import static com.anadollu.mailo.impl.util.Resources.readProperties;

/**
 * Extension Registration and Settings load for CDI environment.
 *
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class MailoExtension implements Extension {

    private SessionConfigBuilder sessionConfigBuilder;

    private SessionConfig sessionConfig;

    public void loadSettings(@Observes AfterDeploymentValidation afterDeploymentValidation, BeanManager beanManager) {

        Properties mailSessionProperties = new Properties();
        try {
            mailSessionProperties = readProperties(SessionConfig.MAIL_SESSION_CONFIG_PROPERTIES_FILE);
        } catch (IOException ignore) {
        }

        sessionConfigBuilder = new SessionConfigBuilderImpl(mailSessionProperties);
        beanManager.fireEvent(new MailSessionConfigurationEvent(sessionConfigBuilder));
        sessionConfig = sessionConfigBuilder.build();
    }

    public SessionConfig getSessionConfig() {
        return sessionConfig;
    }

}
