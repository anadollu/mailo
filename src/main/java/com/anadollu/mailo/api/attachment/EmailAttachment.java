package com.anadollu.mailo.api.attachment;

import com.anadollu.mailo.api.ContentDisposition;
import com.anadollu.mailo.api.Header;

import java.util.Collection;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public interface EmailAttachment {

    public String getContentId();

    public String getFileName();

    public String getMimeType();

    public ContentDisposition getContentDisposition();

    public Collection<Header> getHeaders();

    public byte[] getBytes();
}
