package com.anadollu.mailo.api.attachment;

import com.anadollu.mailo.api.MailException;

/**
 * Thrown when an email address fails to validate as RFC822
 *
 * @author Anatolian ( sertac at anadollu dot com)
 */
public class AttachmentException extends MailException {
    private static final long serialVersionUID = 1L;

    public AttachmentException() {
        super();
    }

    public AttachmentException(String message, Throwable cause) {
        super(message, cause);
    }

    public AttachmentException(String message) {
        super(message);
    }

    public AttachmentException(Throwable cause) {
        super(cause);
    }
}
