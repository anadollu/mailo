package com.anadollu.mailo.api;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public interface EmailContact {

    public String getName();

    public String getAddress();
}
