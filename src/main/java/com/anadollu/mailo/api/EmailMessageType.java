package com.anadollu.mailo.api;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public enum EmailMessageType {
    STANDARD,
    INVITE_ICAL;
}
