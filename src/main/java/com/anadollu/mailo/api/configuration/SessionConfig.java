package com.anadollu.mailo.api.configuration;

import com.anadollu.mailo.api.EmailContact;

/**
 * Bean which holds Mail Session configuration options.
 *
 * @author Anatolian ( sertac at anadollu dot com)
 */
public interface SessionConfig {

    String MAIL_SESSION_CONFIG_PROPERTIES_FILE = "MailSessionConfig.properties";

    String SERVER_HOST = "serverHost";
    String SERVER_PORT = "serverPort";
    String DOMAIN_NAME = "domainName";
    String USERNAME = "username";
    String PASSWORD = "password";
    String ENABLE_TLS = "enableTls";
    String REQUIRE_TLS = "requireTls";
    String ENABLE_SSL = "enableSsl";
    String AUTH = "auth";
    String JNDI_SESSION_NAME = "jndiSessionName";

    String FROM = "from";
    String FROM_NAME = "fromName";
    String REPLY_TO = "replyTo";
    String REPLY_TO_NAME = "replyToName";

    String getServerHost();

    Integer getServerPort();

    String getDomainName();

    String getUsername();

    String getPassword();

    Boolean getEnableTls();

    Boolean getRequireTls();

    Boolean getEnableSsl();

    Boolean getAuth();

    String getJndiSessionName();

    EmailContact getDefaultSender();

    EmailContact getDefaultReplyTo();

}
