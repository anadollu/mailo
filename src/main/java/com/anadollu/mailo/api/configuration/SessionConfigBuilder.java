package com.anadollu.mailo.api.configuration;

import com.anadollu.mailo.impl.configuration.SessionConfigBuilderImpl;

public interface SessionConfigBuilder {

    public static SessionConfigBuilder newInstance() {
        return new SessionConfigBuilderImpl();
    }

    SessionConfigBuilder serverHost(String serverHost);

    SessionConfigBuilder serverPort(int serverPort);

    SessionConfigBuilder domainName(String domainName);

    SessionConfigBuilder username(String username);

    SessionConfigBuilder password(String password);

    SessionConfigBuilder enableTls(boolean enableTls);

    SessionConfigBuilder requireTls(boolean requireTls);

    SessionConfigBuilder enableSsl(boolean enableSsl);

    SessionConfigBuilder auth(boolean auth);

    SessionConfigBuilder jndiSessionName(String jndiSessionName);

    SessionConfigBuilder from(String from);

    SessionConfigBuilder fromName(String fromName);

    SessionConfigBuilder replyTo(String replyTo);

    SessionConfigBuilder replyToName(String replyToName);

    SessionConfig build();

}
