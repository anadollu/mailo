package com.anadollu.mailo.api;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public enum ContentType {
    ALTERNATIVE("alternative"),
    MIXED("mixed"),
    RELATED("related");

    private String value;

    private ContentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
