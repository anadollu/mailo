package com.anadollu.mailo.api;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public enum ICalMethod {
    PUBLISH,
    REQUEST,
    REFRESH,
    CANCEL,
    ADD,
    REPLY,
    COUNTER,
    DECLINECOUNTER;
}
