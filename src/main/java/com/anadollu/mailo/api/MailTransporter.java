package com.anadollu.mailo.api;

/**
 * @author Anatolian ( sertac at anadollu dot com)
 */
public interface MailTransporter {

    public EmailMessage send(EmailMessage emailMessage);
}
