package com.anadollu.mailo.api.event;

import com.anadollu.mailo.api.attachment.EmailAttachment;

import java.util.ArrayList;
import java.util.List;

public class MailEvent {

    private String to;
    private String toName;
    private String subject;
    private String body;
    private List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<EmailAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<EmailAttachment> attachments) {
        this.attachments = attachments;
        if (this.attachments == null) {
            attachments = new ArrayList<EmailAttachment>();
        }
    }

}
