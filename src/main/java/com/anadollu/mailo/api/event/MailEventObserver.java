package com.anadollu.mailo.api.event;

public interface MailEventObserver {

    public void sendEmail(MailEvent event);

}
