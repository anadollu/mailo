package com.anadollu.mailo.api;

import com.anadollu.mailo.api.attachment.EmailAttachment;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;

import javax.mail.internet.InternetAddress;

/**
 * Base interface for creating email messages.
 *
 * @author Anatolian ( sertac at anadollu dot com)
 */
public interface MailMessage {

    // Begin Recipients

    /**
     * Convenience varargs method to add FROM address(es)
     *
     * @param address Address of the recipient eq "john.doe@example.com" or "John Doe<john.doe@example.com>"
     * @throws InvalidAddressException if address(es) are in an invalid format
     */
    public MailMessage from(String... address);

    /**
     * Adds a From Address
     *
     * @param emailAddress {@link InternetAddress} of the address to be added
     */
    public MailMessage from(InternetAddress emailAddress);

    /**
     * Adds a From Address
     *
     * @param emailContact {@link EmailContact} of the address to be added
     */
    public MailMessage from(EmailContact emailContact);

    /**
     * Adds a Collection of {@link EmailContact} as FROM addresses
     *
     * @param emailContacts Collection of {@link EmailContact} to be added
     */
    public MailMessage from(Collection<? extends EmailContact> emailContacts);

    /**
     * Convenience varargs method to add REPLY-TO address(es)
     *
     * @param address Address of the recipient eq "john.doe@example.com" or "John Doe<john.doe@example.com>"
     * @throws InvalidAddressException if address(es) are in an invalid format
     */
    public MailMessage replyTo(String... address);

    /**
     * Adds a REPLY-TO Address
     *
     * @param emailAddress {@link InternetAddress} of the address to be added
     */
    public MailMessage replyTo(InternetAddress emailAddress);

    /**
     * Adds a REPLY-TO Address
     *
     * @param emailContact {@link EmailContact} of the address to be added
     */
    public MailMessage replyTo(EmailContact emailContact);

    /**
     * Adds a Collection of {@link EmailContact} as REPLY-TO addresses
     *
     * @param emailContacts Collection of {@link EmailContact} to be added
     */
    public MailMessage replyTo(Collection<? extends EmailContact> emailContacts);

    /**
     * Add header to the message.
     *
     * @param name  Header name
     * @param value Header value
     */
    public MailMessage addHeader(String name, String value);

    /**
     * Convenience varargs method to add TO address(es)
     *
     * @param address Address of the recipient eq "john.doe@example.com" or "John Doe<john.doe@example.com>"
     * @throws InvalidAddressException if address(es) are in an invalid format
     */
    public MailMessage to(String... address);

    /**
     * Add TO recipient
     *
     * @param emailAddress {@link InternetAddress} of the address to be added
     */
    public MailMessage to(InternetAddress emailAddress);

    /**
     * Add TO recipient
     *
     * @param emailContact {@link EmailContact} of the address to be added
     */
    public MailMessage to(EmailContact emailContact);

    /**
     * Convenience method to add a TO recipients
     *
     * @param emailContacts Collection of {@link EmailContact} to be added
     */
    public MailMessage to(Collection<? extends EmailContact> emailContacts);

    /**
     * Convenience varargs method to add CC address(es)
     *
     * @param address Address of the recipient eq "john.doe@example.com" or "John Doe<john.doe@example.com>"
     * @throws InvalidAddressException if address(es) are in an invalid format
     */
    public MailMessage cc(String... address);

    /**
     * Add CC (Carbon Copy) recipient
     *
     * @param emailAddress {@link InternetAddress} of the address to be added
     */
    public MailMessage cc(InternetAddress emailAddress);

    /**
     * Add CC recipient
     *
     * @param emailContact {@link EmailContact} of the address to be added
     */
    public MailMessage cc(EmailContact emailContact);

    /**
     * Add collection of CC (Carbon Copy) recipients
     *
     * @param emailContacts Collection of {@link EmailContact} to be added
     */
    public MailMessage cc(Collection<? extends EmailContact> emailContacts);

    /**
     * Convenience varargs method to add BCC address(es)
     *
     * @param address Address of the recipient eq "john.doe@example.com" or "John Doe<john.doe@example.com>"
     * @throws InvalidAddressException if address(es) are in an invalid format
     */
    public MailMessage bcc(String... address);

    /**
     * Add BCC (Blind Carbon Copy) recipient
     *
     * @param emailAddress {@link InternetAddress} of the address to be added
     */
    public MailMessage bcc(InternetAddress emailAddress);

    /**
     * Add BCC recipient
     *
     * @param emailContact {@link EmailContact} of the address to be added
     */
    public MailMessage bcc(EmailContact emailContact);

    /**
     * Add collection of BCC (Blind Carbon Copy) recipients
     *
     * @param emailContacts Collection of {@link EmailContact} to be added
     */
    public MailMessage bcc(Collection<? extends EmailContact> emailContacts);

    /**
     * Set the "Envelope From" address which is used for error messages
     */
    public MailMessage envelopeFrom(EmailContact emailContact);

    /**
     * Set the "Envelope From" address which is used for error messages
     */
    public MailMessage envelopeFrom(String address);

    // End Recipients

    // Begin Attachments

    /**
     * Adds Attachment to the message
     *
     * @param attachment {@link EmailAttachment} to be added
     */
    public MailMessage addAttachment(EmailAttachment attachment);

    /**
     * Adds a Collection of Attachments to the message
     */
    public MailMessage addAttachments(Collection<? extends EmailAttachment> attachments);

    /**
     * Adds Attachment to the message
     */
    public MailMessage addAttachment(String fileName, String mimeType, ContentDisposition contentDispostion,
                                     byte[] bytes);

    /**
     * Adds Attachment to the message
     */
    public MailMessage addAttachment(String fileName, String mimeType, ContentDisposition contentDispostion,
                                     InputStream inputStream);

    /**
     * Adds Attachment to the message
     */
    public MailMessage addAttachment(ContentDisposition contentDispostion, File file);

    // End Attachements

    // Begin Flags

    /**
     * Sets the importance level of the message with a given {@link MessagePriority}
     *
     * @param messagePriority The priority level of the message.
     */
    public MailMessage importance(MessagePriority messagePriority);

    /**
     * Request a delivery receipt "Return-Receipt-To" to the given address
     *
     * @param address Email address the receipt should be sent to
     * @throws InvalidAddressException if address is in invalid format
     */
    public MailMessage deliveryReceipt(String address);

    /**
     * Request a read receipt "Disposition-Notification-To" to a given address
     *
     * @param address Email address the receipt should be sent to
     * @throws InvalidAddressException if address is in invalid format
     */
    public MailMessage readReceipt(String address);

    /**
     * Set the Message-ID for the message.
     */
    public MailMessage messageId(String messageId);

    // End Flags

    // Begin Calendar

    /**
     * Used for creating iCal Calendar Invites.
     *
     * @param textBody   Summary of the invite to be displayed in the body of the email messages.
     * @param ICalMethod iCalMethod
     * @param bytes      iCal data which will be attached to the message
     */
    public MailMessage iCal(String textBody, ICalMethod method, byte[] bytes);

    /**
     * Used for creating iCal Calendar Invites.
     *
     * @param htmlBody   Summary of the invite to be displayed in the html body of the email messages.
     * @param textBody   Summary of the invite to be displayed in the alternate text body of the email messages.
     * @param ICalMethod iCalMethod
     * @param bytes      iCal data which will be attached to the message
     */
    public MailMessage iCal(String htmlBody, String textBody, ICalMethod method, byte[] bytes);

    // End Calendar

    // Begin Core

    /**
     * Set the subject on the message
     *
     * @param value Subject of the message
     */
    public MailMessage subject(String value);

    /**
     * Sets the body of the message a plan text body represented by the supplied string
     *
     * @param text Plain text body
     */
    public MailMessage bodyText(String text);

    /**
     * Sets the body of the message a HTML body represented by the supplied string
     *
     * @param html HTML body
     */
    public MailMessage bodyHtml(String html);

    /**
     * Sets the body of the message to a HTML body with a plain text alternative
     *
     * @param html HTML body
     * @param text Plain text body
     */
    public MailMessage bodyHtmlTextAlt(String html, String text);

    /**
     * Set the charset of the message. Otherwise defaults to Charset.defaultCharset()
     */
    public MailMessage charset(String charset);

    /**
     * Set the Content Type of the message
     */
    public MailMessage contentType(ContentType contentType);

    // End Core

    /**
     * Get the {@link EmailMessage} representing this {@link MailMessage}
     *
     * @return {@link EmailMessage} representing this {@link MailMessage}
     */
    public EmailMessage getEmailMessage();

    /**
     * Set the {@link EmailMessage} representing this {@link MailMessage}
     */
    public void setEmailMessage(EmailMessage emailMessage);

    /**
     * Send the Message
     *
     * @return {@link EmailMessage} which represents the {@link MailMessage} as sent
     * @throws SendFailedException If the messages fails to be sent.
     */
    public EmailMessage send();

}
